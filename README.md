# Allergio

Allergio é uma API desenvolvida para fornecer aos brasileiros uma forma de expor suas reações adversas e alérgicas para otimizar o atendimento em qualquer cidade brasileira.

Para isso, conta com quatro tipos de usuários:
* **Usuário Alvo**: responsável por cadastrar no sistema suas reações alérgicas, bem como quais medicamentos está consumindo;
* **Administrador**: responsável por cadastrar no sistema medicamentos e princípios ativos, possibilitando que usuários cadastrem alergias. Além disso, é responsável por cadastrar um hospital com um diretor, descrito abaixo;
* **Diretor**: responsável, no sistema, por um hospital. Quando um usuário deseja informar ao sistema que é um agente de saúde, ele deve associar-se a um hospital. Ao fazê-lo, o diretor do hospital será notificado e deve informar se a pessoa é, de fato, um agente de saúde associado ao hospital. Por razões de segurança, é possível verificar quem aceitou um agente de saúde na aplicação;
* **Agente de Saúde**: Um agente de saúde pode visualizar as reações adversas e medicamentos consumidos que um usuário cadastrou, bem como cadastrar qualquer uma dessas interações. É importante salientar que, por motivos de segurança, toda consulta que um agente de saúde realiza é notificada ao usuário pesquisado;

Esse sistema é uma proposta que visa adoção governamental brasileira. Por isso, o usuário alvo é um brasileiro, sendo que o administrador, idealmente, é um funcionário associado ao Ministério da Saúde e ANVISA para o cadastro de medicamentos e hospitais.

Esta documentação encontra-se na versão 1.0.
O _software_ aqui descrito encontra-se na versão 1.0.

### Leituras Recomendadas

* [Guia de Uso](docs/guides/use_guide.md)
* [Documentação Técnica](docs/tecnical_documentation/documentation_home.md)
* [Notícias](news.md)
* [Sugestões e _feedback_](feedback.md)

### Licença

MIT

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
