class LoginSerializer < ActiveModel::Serializer
    attributes :id, :first_name, :birth, :last_login_at, :is_sysadmin, :is_medic, :logs
    attribute :token

    def token
        @instance_options[:token]
    end

    def is_medic
        object.is_medic?
    end
end
