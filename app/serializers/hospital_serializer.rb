class HospitalSerializer < ActiveModel::Serializer
  attributes :id, :name, :cnpj, :state, :user_id

  belongs_to :user do
    link(:related) { user_path(object.user_id) }
  end

  link(:self) { hospital_path(object.id) }
end
