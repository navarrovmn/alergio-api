class MedicineSerializer < ActiveModel::Serializer
  attributes :id, :name, :register

  has_many :substances

  link(:self) { medicine_path(object.id) }
end
