class SubstanceSerializer < ActiveModel::Serializer
  attributes :id, :name

  link(:self) { substance_path(object.id) }
end
