class SafetySerializer < ActiveModel::Serializer
  attributes :id, :name, :register
  attribute :safe_to_consume

  link(:self) { medicine_path(object.id) }

  def safe_to_consume
    @instance_options[:safe_to_consume]
  end
end
