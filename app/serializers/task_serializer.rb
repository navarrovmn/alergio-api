class TaskSerializer < ActiveModel::Serializer
  attributes :id, :accepted

  has_one :user do
    link(:related) { user_path(object.user_id) }
  end

  has_one :license do
    link(:related) { user_licenses_path(object.license.user_id) }
  end
end
