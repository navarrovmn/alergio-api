class LicenseSerializer < ActiveModel::Serializer
  attributes :id, :license_type, :number, :approved, :state, :name, :cpf

  belongs_to :user do
    link(:related) { user_path(object.user_id) }
  end

  def name
    object.user.first_name
  end

  def cpf
    object.user.cpf
  end
end
