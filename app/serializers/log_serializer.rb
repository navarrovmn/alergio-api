class LogSerializer < ActiveModel::Serializer
  attributes :id, :created_at

  belongs_to :user do
    link(:related) { user_path(object.user_id) }
  end
end
