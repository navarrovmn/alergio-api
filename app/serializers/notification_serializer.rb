class NotificationSerializer < ActiveModel::Serializer
  attributes :id, :carer_id, :patient_id, :seen_at

  belongs_to :carer do
    link(:related) { user_path(object.carer_id) }
  end

  belongs_to :patient do
    link(:related) { user_path(object.patient_id) }
  end
end
