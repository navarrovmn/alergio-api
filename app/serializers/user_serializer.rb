class UserSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :birth, :last_login_at, :is_sysadmin, :email

  has_many :occurrences do
    link(:related) { user_notifications_path(object.id) }
  end

  has_many :interactions do
    link(:related) { user_interactions_path(object.id) }
  end

  has_many :tasks do
    link(:related) { user_tasks_path(object.id) }
  end

  has_many :logs do
    link(:related) { user_logs_path(object.id) }
  end

  has_one :license do
    link(:related) { user_licenses_path(object.id) }
  end

  has_one :hospital do
    if object.hospital
      link(:related) { hospital_path(object.hospital.id) }
    end
  end
end
