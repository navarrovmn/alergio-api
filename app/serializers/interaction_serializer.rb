class InteractionSerializer < ActiveModel::Serializer
  attributes :id, :user_id, :substance_id, :adverse, :description, :inserted_by_self, :substance_name

  def substance_name
    object.substance.name
  end

  belongs_to :user do
    link(:related) { user_path(object.user_id) }
  end
end
