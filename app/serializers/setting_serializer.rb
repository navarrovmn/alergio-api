class SettingSerializer < ActiveModel::Serializer
  attributes :id, :time_to_change_password, :maximum_login_tries, :time_to_invalidate_token
end
