class Substance < ApplicationRecord
    validates :name, presence: true, uniqueness: true

    has_and_belongs_to_many :medicines
    has_many :interactions
    has_many :users, through: :interactions
end
