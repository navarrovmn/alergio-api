class License < ApplicationRecord
    include States

    belongs_to :user
    has_one :task

    validates :license_type, :number, :state, presence: true
    validates_inclusion_of :license_type, in: 0..1
    validates :number, length: { maximum: 20 }, uniqueness: true
    validate :state_validation

    private

        def state_validation
            if !States::state_valid? state
                errors.add(:state, "State is not valid. Check spelling or formatting")
            end
        end
end
