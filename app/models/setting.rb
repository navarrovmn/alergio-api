class Setting < ApplicationRecord
    validates :singleton_guard, uniqueness: true
    validates_inclusion_of :singleton_guard, in: [0]
    validate :custom_validations

    def self.instance
        return @instance if @instance

        unless @instance = Setting.first
            @instance = Setting.create
        end

        @instance
    end

    private

        def custom_validations
            if time_to_change_password < 6
                errors.add(:time_to_change_password, "Must be minimum 6 months")
            end
            if maximum_login_tries > 10
                errors.add(:maximum_login_tries, "Maximum login tries must be 10 at maximum")
            end
            if time_to_invalidate_token < 5 || time_to_invalidate_token > 20
                errors.add(:time_to_invalidate_token, "Time to invalidate token is out of range. Must be between 5 and 20")
            end
        end
end