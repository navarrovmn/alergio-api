class User < ApplicationRecord
    VALID_EMAIL = /\A[a-zA-Z0-9.!\#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*\z/

    validates :cpf, presence: true, length: { is: 11 }, uniqueness: true
    validates :email, presence: true, uniqueness: true, format: { with: VALID_EMAIL }
    validates :first_name, presence: true, length: { minimum: 2 }
    validates :birth, presence: true
    validates :last_changed_password_at, presence: true
    validates :password, length: { minimum: 8 }, presence: true, on: :create
    validate :password_validation, on: :create
    validate :name_validation, :cpf_validation

    has_many :occurrences, :class_name => "Notification", :foreign_key => "patient_id"
    has_many :queries, :class_name => "Notification", :foreign_key => "carer_id"
    has_many :interactions
    has_many :tasks
    has_many :logs
    has_many :substances, through: :interactions
    has_one :hospital
    has_one :license

    has_secure_password

    def is_medic?
        !self.license.nil? && self.license.approved
    end

    def is_director?
        !self.hospital.nil?
    end

    def is_safe?(medicine)
        substances = self.interactions.where(adverse: true).pluck(:substance_id)
        med_substances = medicine.substances.pluck(:substance_id)

        return true if (substances & med_substances).empty?
        false
    end

    private
        def password_validation
            unless password =~ /[[:alpha:]]/ && password =~ /[[:digit:]]/
                errors.add(:password, "Must have alphabetic chars and digits")
            end
        end

        def name_validation
            if first_name.strip.include? " "
                errors.add(:first_name, "Must receive just one name")
            end
        end

        def cpf_validation
            if !CPF.valid? cpf
                errors.add(:cpf, "CPF is not valid")
            end
        end
end
