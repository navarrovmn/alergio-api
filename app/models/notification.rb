class Notification < ApplicationRecord
    validates :carer, :patient, :seen_at, presence: true

    belongs_to :carer, :class_name => "User"
    belongs_to :patient, :class_name => "User"
end
