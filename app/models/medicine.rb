class Medicine < ApplicationRecord
    validates :register, presence: true, uniqueness: true, length: { is: 9 }
    validates :name, presence: true

    has_and_belongs_to_many :substances
end
