class Interaction < ApplicationRecord
    belongs_to :user
    belongs_to :substance
    validate :adverse_field_validation

    private

    def adverse_field_validation
        if adverse != false && adverse != true
            errors.add(:adverse, "Adverse can't be blank")
        end
    end
end
