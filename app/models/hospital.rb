class Hospital < ApplicationRecord
    include States

    belongs_to :user

    validates :name, :cnpj, :state, presence: true
    validates :cnpj, uniqueness: true
    validate :state_validation, :cnpj_validation, :user_validation

    private
        def cnpj_validation
            if !CNPJ.valid? cnpj
                errors.add(:cnpj, "CNPJ is not valid")
            end
        end

        def state_validation
            if !States::state_valid? state
                errors.add(:state, "State is not valid. Check spelling or formatting")
            end
        end

        def user_validation
            if user && !user.is_medic?
                errors.add(:user, "User must be a carer to be a director")
            end
        end
end
