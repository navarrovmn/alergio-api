class Task < ApplicationRecord
  belongs_to :user
  belongs_to :license

  def close(option)
    license = License.find(self.license_id)

    self.update_attributes(accepted: option)
    license.update_attributes(approved: option)
  end
end
