class SubstancesController < ApplicationController
  before_action :is_sysadmin?, only: [:create, :update, :destroy]
  before_action :set_substance, only: [:show, :update, :destroy]

  def show
    render json: @substance
  end

  def index
    if(params[:substance_name])
      @substances = Substance.where('name LIKE ?', "%#{params[:substance_name]}%")
    else
      @substances = Substance.all
    end

    render json: @substances
  end

  def update
    if @substance.update(substance_params)
      render json: @substance
    else
      render json: @substance.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @substance.destroy
  end

  private
    def set_substance
      @substance = Substance.find(params[:id])
    end

    def substance_params
      params.require(:substance).permit(:name)
    end
end
