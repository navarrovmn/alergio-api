class MedicinesController < ApplicationController
  before_action :is_sysadmin?, only: [:create, :destroy, :update]
  before_action :set_medicine, only: [:show, :update, :destroy]
  before_action :check_params, only: [:create]

  def index
    if(params[:medicine_name])
      @medicines = Medicine.where('name LIKE ?', "%#{params[:medicine_name]}%")
    else
      @medicines = Medicine.all
    end

    render json: @medicines
  end

  def show
    render json: @medicine
  end

  def create
    @medicine = Medicine.new(medicine_params.except(:substances_attributes))

    if @medicine.valid?
      medicine_params[:substances_attributes].each do |substance|
        new_substance = Substance.find_or_create_by(name: substance[:name])
        @medicine.substances << new_substance
      end

      @medicine.save
      render json: @medicine, status: :created, location: @medicine
    else
      render json: @medicine.errors, status: :unprocessable_entity
    end
  end

  def update
    if @medicine.update(medicine_params)
      render json: @medicine
    else
      render json: @medicine.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @medicine.destroy
  end

  private
    def set_medicine
      @medicine = Medicine.find(params[:id])
    end

    def check_params
      if !params[:medicine][:substances_attributes].present? || params[:medicine][:substances_attributes].empty?
        render_error "Bad params", "Substances must be passed to create medicine", :bad_request
      end
    end

    def medicine_params
      params.require(:medicine).permit(:name, :register, substances_attributes: [:name])
    end
end
