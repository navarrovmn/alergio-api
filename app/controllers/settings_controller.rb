class SettingsController < ApplicationController
  before_action :is_sysadmin?

  def show
    render json: Setting.instance
  end

  def update
    if Setting.instance.update(setting_params)
        render json: Setting.instance
    else
        render json: Setting.instance.errors, status: :unprocessable_entity
    end
  end

  private

    def setting_params
      params.require(:setting).permit(:time_to_change_password, :maximum_login_tries, :time_to_invalidate_token)
    end
end