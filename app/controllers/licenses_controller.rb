class LicensesController < ApplicationController
  before_action :set_license, only: [:show]

  def create
    if params[:cnpj]
      hospital = Hospital.find_by(cnpj: params[:cnpj])
      @license = License.new(license_params)
      @license.user = @request_user

      if hospital and @license.save
        @task = Task.create(license_id: @license.id, user_id: hospital.user.id)

        render json: @license, status: :created
      else
        if hospital == nil
          render_error "Wrong params", "Could not find hospital", :bad_request
        else
          render @license.errors, status: :unprocessable_entity
        end
      end
    else
      render_error "Wrong params", "CNPJ was not given", :bad_request
    end
  end

  def show
    render json: @license
  end

  private
    def set_license
      @license = User.find(params[:user_id]).license
    end

    def license_params
      params.require(:license).permit(:license_type, :number, :state)
    end
end
