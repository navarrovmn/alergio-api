module Permission
    extend ActiveSupport::Concern

    def has_access?
        if !is_owner_or_carer?
            render_error "Unauthorized", "You do not own this information", :unauthorized
        end
    end

    def is_sysadmin?
        if !@request_user.is_sysadmin
            render_error "Unauthorized", "Must be sysadmin to perform this action", :unauthorized
        end
    end

    def is_owner_or_carer?
        if params[:user_id].present? || (params[:id].present? && params[:controller] == "users")
            id = params[:user_id] || params[:id]
            if @request_user.is_medic? && PERMITTED_CARE_ACTIONS.include?(@request_action)
                true
            elsif @request_user.is_sysadmin && PERMITTED_ADMIN_ACTIONS.include?(@request_action)
                true
            else
                User.find(id) == @request_user
            end
        else
            true
        end
    end

    private
        PERMITTED_CARE_ACTIONS = [
            "interactions#create",
            "interactions#index",
            "users#show"
        ].freeze

        PERMITTED_ADMIN_ACTIONS = [
            "users#update",
            "users#show"
        ].freeze
end