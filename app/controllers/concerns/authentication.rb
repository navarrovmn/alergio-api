module Authentication
    extend ActiveSupport::Concern

    def login
        @user = User.find_by(email: params[:email])

        if @user
            if @user.authenticate(params[:password])
                render json: @user, serializer: LoginSerializer, token: generate_token, status: :created
                @user.logs.delete_all
            else
                if should_block_user?
                    @user.update_attribute(:blocked, true)

                    render_error "Authentication", "Your user is blocked", :forbidden
                else
                    render_error "Authentication", "It was not possible to login", :unauthorized
                end
            end
        else
            render_error "Authentication", "It was not possible to login", :bad_request
        end
    end

    def is_authenticated
         if request.headers['Authentication'].present?
            decoded_token = decode(request.headers['Authentication'])

            if decoded_token.size == 2
                @request_user = User.find(decoded_token.first["user_id"])
            else
                render_error *decoded_token.first
            end
        else
            render_error "Authentication", "No token was given", :unauthorized
        end
    end

    def must_change_password?
        @request_action = params[:controller] + "#" + params[:action]

        if @request_user.last_changed_password_at < Setting.instance.time_to_change_password.months.ago
            @request_user.update_attribute(:must_change_password, true)
        end

        if @request_user.must_change_password && @request_action != "users#update"
            render_error "Password", "You must change your password", :forbidden
        end
    end

    private

    # exp_time is the time in minutes which token will be expired

    def encode(payload = {}, exp_time = 20)
        payload.merge!(exp: Time.now.to_i + exp_time * 60)
        JWT.encode(payload, Rails.application.credentials[:hmac_secret] || ENV['TEST_KEY'], 'HS256')
    end

    def decode(token)
        begin
            JWT.decode(token, Rails.application.credentials[:hmac_secret] || ENV['TEST_KEY'], true, { algorithm: 'HS256' })
        rescue JWT::ExpiredSignature
            [["Authentication", "Token has expired", :unauthorized]]
        rescue
            [["Authentication", "Invalid token", :unauthorized]]
        end
    end

    def generate_token
        @user.update_attribute(:last_login_at, Time.now)

        auth_token = encode({ user_id: @user.id },  Setting.instance.time_to_invalidate_token)
    end

    def should_block_user?
        @user.logs.create
        @user.logs.count > Setting.instance.maximum_login_tries
    end
end