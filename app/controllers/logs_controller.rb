class LogsController < ApplicationController

  def index
    @logs = User.find(params[:user_id]).logs

    render json: @logs
  end
end
