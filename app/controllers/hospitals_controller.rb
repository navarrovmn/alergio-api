class HospitalsController < ApplicationController
    skip_before_action :has_access?
    before_action :set_hospital, only: [:update, :show]
    before_action :is_hospital_director?, only: [:update]
    before_action :is_sysadmin?, only: [:create]

  def index
    @hospitals = Hospital.all

    render json: @hospitals
  end

  def show
    render json: @hospital
  end

  def create
    if params[:cpf]
      new_director = User.find_by(cpf: params[:cpf])
      if new_director
        @hospital = Hospital.new(create_hospital_params.except(:license_attributes))
        @license = License.new(create_hospital_params[:license_attributes])

        @hospital.user = new_director
        @license.user = new_director

        @license.save
        @hospital.save
      else
        render_error "Wrong params", "Could not find user with CPF given", :bad_request
        return
      end
1    else
      @hospital = Hospital.new(create_hospital_params.except(:license_attributes))
      @license = License.create(create_hospital_params[:license_attributes])
    end

    if @license && !@license.valid?
        render json: @license.errors, status: :unprocessable_entity
    else
        if @hospital.valid?
            @hospital.save
            render json: @hospital, status: :created, location: @hospital
        else
            render json: @hospital.errors, status: :unprocessable_entity
        end
    end
  end

  def update
    if @hospital.update(update_hospital_params)
        render json: @hospital
    else
        render json: @hospital.errors, status: :unprocessable_entity
    end
  end

  private
    def set_hospital
      if params[:id].size == 14
        @hospital = Hospital.find_by(cnpj: params[:id])
      else
        @hospital = Hospital.find(params[:id])
      end
    end

    def create_hospital_params
      params.require(:hospital).permit(
        :name,
        :cnpj,
        :state,
        :user_id,
        license_attributes: [
          :license_type,
          :number,
          :state,
          :approved,
          :user_id
        ]
      )
    end

    def update_hospital_params
        params.require(:hospital).permit(:name, :user_id)
    end

    def is_hospital_director?
        if @hospital.user != @request_user
            render_error "Unpermitted action", "Only director can update hospital", :unauthorized
        end
    end

end
