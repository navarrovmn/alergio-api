class TasksController < ApplicationController
  before_action :set_task, except: [:index]

  def index
    @tasks = Task.includes(:license).where(user_id: params[:user_id]).where(accepted: nil)

    render json: @tasks, include: ['license']
  end

  def update
    if @task.accepted == nil
      @task.close(params[:task][:accepted])

      render json: @task
    else
      render_error "Not permitted", "This task has already been closed", :unprocessable_entity
    end
  end

  private
    def set_task
      @task = Task.find(params[:id])
    end

    def task_params
      params.require(:task).permit(:accepted)
    end
end
