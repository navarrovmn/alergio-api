class ApplicationController < ActionController::API
    include Render
    include Authentication
    include Permission

    before_action :is_authenticated, except: [:login]
    before_action :must_change_password?, except: [:login]
    before_action :has_access?, except: [:login]

    skip_before_action :is_authenticated, only: [:welcome]
    skip_before_action :must_change_password?, only: [:welcome]
    skip_before_action :has_access?, only: [:welcome]

    def welcome
        render json: { "Allergio": "v1.0" }
    end
end
