class UsersController < ApplicationController
  wrap_parameters :user, include: [
    :cpf,
    :email,
    :first_name,
    :birth,
    :password,
    :password_confirmation,
    :must_change_password,
    :blocked
  ]

  skip_before_action :is_authenticated, only: [:create]
  skip_before_action :has_access?, only: [:create]
  skip_before_action :must_change_password?, only: [:create]
  before_action :set_user, only: [:show, :update, :check_medicine]
  before_action :is_sysadmin?, only: [:force_password_change, :unblock_user]

  def show
    render json: @user
  end

  def create
    @user = User.new(user_params)
    @user.last_changed_password_at = Time.now

    if @user.save
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def update
    if @request_user.is_sysadmin && @user != @request_user
      update_by_sysadmin
    else
      update_by_user
    end
  end

  def update_by_sysadmin
    valid_fields = true unless (params[:must_change_password] == false || params[:blocked] == true)

    if valid_fields && @user.update(user_params)
      render json: @user
    else
      render @user.errors, status: :unprocessable_entity
    end
  end

  def update_by_user
    if @user.authenticate(user_params[:password])
      @user.errors.add(:password, "New password must be different")

      render json: @user.errors, status: :unprocessable_entity
    else
      @user.must_change_password = false if (@user.must_change_password && !user_params[:password].blank?)
      if @user.update(user_params)
        render json: @user
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    end
  end

  def check_medicine
    @medicine = Medicine.find(params[:medicine_id])
    safety = @user.is_safe?(@medicine)

    render json: @medicine, serializer: SafetySerializer, safe_to_consume: safety
  end

  private
    def set_user
      if(params[:id].size == 11)
        @user = User.find_by(cpf: params[:id])
      else
        @user = User.find(params[:id])
      end
    end

    def user_params
      if @request_user && @request_user.is_sysadmin && @request_user != @user
        params.require(:user).permit(:must_change_password, :blocked)
      elsif @request_user
        params.require(:user).permit(:first_name, :email, :password, :password_confirmation)
      else
        params.require(:user).permit(:cpf, :email, :first_name, :birth, :password, :password_confirmation)
      end
    end
end
