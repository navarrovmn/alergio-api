class InteractionsController < ApplicationController
  before_action :set_interaction, only: [:update, :destroy]

  def index
    if params[:user_id].size == 11
      @user = User.find_by(cpf: params[:user_id])
    else
      @user = User.find(params[:user_id])
    end

    if @user != @request_user
      Notification.create(patient: @user, carer: @request_user, seen_at: Time.now)
    end

    @interactions = Interaction.includes(:substance).where(user_id: @user.id)

    render json: @interactions
  end

  def create
    @interaction = Interaction.new(interaction_params)
    @interaction.user = User.find(params[:user_id])
    @interaction.inserted_by_self = false if @request_user.id != params[:user_id].to_i

    if @interaction.save
      render json: @interaction, status: :created
    else
      render json: @interaction.errors, status: :unprocessable_entity
    end
  end

  def update
    if @interaction.update(update_params)
      render json: @interaction
    else
      render json: @interaction.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @interaction.destroy
  end

  private

    def set_interaction
      @interaction = Interaction.find(params[:id])
    end

    def interaction_params
      params.require(:interaction).permit(:user_id, :substance_id, :adverse, :description)
    end

    def update_params
      params.require(:interaction).permit(:description)
    end
end
