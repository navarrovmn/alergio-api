class NotificationsController < ApplicationController

  def index
    @user = User.find(params[:user_id])

    if @user
      @notifications = @user.occurrences

      render json: @notifications, status: :ok
    else
      render_error "Identification", "No id given", :bad_request
    end
  end

end
