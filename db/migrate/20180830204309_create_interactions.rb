class CreateInteractions < ActiveRecord::Migration[5.2]
  def change
    create_table :interactions do |t|
      t.integer :user_id
      t.integer :substance_id
      t.boolean :adverse
      t.string :description

      t.timestamps
    end
  end
end
