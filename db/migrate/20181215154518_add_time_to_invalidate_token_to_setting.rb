class AddTimeToInvalidateTokenToSetting < ActiveRecord::Migration[5.2]
  def change
    add_column :settings, :time_to_invalidate_token, :integer, default: 5
  end
end
