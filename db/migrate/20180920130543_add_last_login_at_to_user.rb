class AddLastLoginAtToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :last_login_at, :date
  end
end
