class AddUserToLicenses < ActiveRecord::Migration[5.2]
  def change
    add_reference :licenses, :user, foreign_key: true, unique: true
  end
end
