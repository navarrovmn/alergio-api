class CreateSubstancesMedicines < ActiveRecord::Migration[5.2]
  def change
    create_table :medicines_substances, id: false do |t|
      t.belongs_to :substance, index: true
      t.belongs_to :medicine, index: true
    end
  end
end
