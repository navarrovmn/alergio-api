class CreateLicenses < ActiveRecord::Migration[5.2]
  def change
    create_table :licenses do |t|
      t.integer :license_type
      t.string :number
      t.boolean :approved
      t.string :state

      t.timestamps
    end
  end
end
