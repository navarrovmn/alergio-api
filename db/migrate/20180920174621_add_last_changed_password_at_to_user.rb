class AddLastChangedPasswordAtToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :last_changed_password_at, :datetime
  end
end
