class AddInsertedBySelfToInteraction < ActiveRecord::Migration[5.2]
  def change
    add_column :interactions, :inserted_by_self, :boolean, default: true
  end
end
