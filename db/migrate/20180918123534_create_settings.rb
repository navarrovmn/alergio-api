class CreateSettings < ActiveRecord::Migration[5.2]
  def change
    create_table :settings do |t|
      t.integer :singleton_guard, default: 0
      t.integer :time_to_change_password, default: 6
    end
  end
end
