class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :cpf
      t.string :email
      t.string :first_name
      t.date :birth
      t.boolean :is_sysadmin, :default => false

      t.timestamps
    end
  end
end
