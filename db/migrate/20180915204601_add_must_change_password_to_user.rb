class AddMustChangePasswordToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :must_change_password, :boolean, :default => false
  end
end
