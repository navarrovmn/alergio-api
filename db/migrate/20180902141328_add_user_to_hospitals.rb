class AddUserToHospitals < ActiveRecord::Migration[5.2]
  def change
    add_reference :hospitals, :user, foreign_key: true, unique: true
  end
end
