class AddMaximumLoginTriesToSetting < ActiveRecord::Migration[5.2]
  def change
    add_column :settings, :maximum_login_tries, :integer, default: 5
  end
end
