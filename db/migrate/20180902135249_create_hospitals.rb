class CreateHospitals < ActiveRecord::Migration[5.2]
  def change
    create_table :hospitals do |t|
      t.string :name
      t.string :cnpj
      t.string :state

      t.timestamps
    end
  end
end
