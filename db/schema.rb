# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_12_15_154518) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "hospitals", force: :cascade do |t|
    t.string "name"
    t.string "cnpj"
    t.string "state"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_hospitals_on_user_id"
  end

  create_table "interactions", force: :cascade do |t|
    t.integer "user_id"
    t.integer "substance_id"
    t.boolean "adverse"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "inserted_by_self", default: true
  end

  create_table "licenses", force: :cascade do |t|
    t.integer "license_type"
    t.string "number"
    t.boolean "approved"
    t.string "state"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_licenses_on_user_id"
  end

  create_table "logs", force: :cascade do |t|
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_logs_on_user_id"
  end

  create_table "medicines", force: :cascade do |t|
    t.string "name"
    t.string "register"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medicines_substances", id: false, force: :cascade do |t|
    t.bigint "substance_id"
    t.bigint "medicine_id"
    t.index ["medicine_id"], name: "index_medicines_substances_on_medicine_id"
    t.index ["substance_id"], name: "index_medicines_substances_on_substance_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.integer "carer_id"
    t.integer "patient_id"
    t.datetime "seen_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "settings", force: :cascade do |t|
    t.integer "singleton_guard", default: 0
    t.integer "time_to_change_password", default: 6
    t.integer "maximum_login_tries", default: 5
    t.integer "time_to_invalidate_token", default: 5
  end

  create_table "substances", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tasks", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "license_id"
    t.boolean "accepted"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["license_id"], name: "index_tasks_on_license_id"
    t.index ["user_id"], name: "index_tasks_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "cpf"
    t.string "email"
    t.string "first_name"
    t.date "birth"
    t.boolean "is_sysadmin", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.boolean "must_change_password", default: false
    t.boolean "blocked", default: false
    t.date "last_login_at"
    t.datetime "last_changed_password_at"
  end

  add_foreign_key "hospitals", "users"
  add_foreign_key "licenses", "users"
  add_foreign_key "logs", "users"
  add_foreign_key "tasks", "licenses"
  add_foreign_key "tasks", "users"
end
