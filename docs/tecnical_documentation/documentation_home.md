## Documentação Técnica

A API foi escrita utilizando a linguagem Ruby com o _framework_ Ruby on Rails, na versão 5.2.1. Toda a documentação técnica está escrita partindo do princípio de que a pessoa lendo a documentação possui um conhecimento intermediário de Ruby on Rails.

A seguir, você poderá ler sobre a modelagem do banco da aplicação, bem como todos os _endpoints_ presentes na mesma e suas respectivas particularidades. Mas antes, é necessário comentar como realizar _login_ na aplicação, isso é, como funciona a autenticação.

A autenticação na aplicação é realizada utilizando _Json Web Token_ (JWT). A documentação referente ao JWT pode ser encontrada [aqui](https://jwt.io/). A implementação dessa tecnologia utilizada foi a _gem_ ruby-jwt, que está documentada [aqui](https://github.com/jwt/ruby-jwt).

Assumindo que uma pessoa já está cadastrada, ela deve fazer uma requisição para '/sessions' com o seguinte corpo:

```
{
  'email': emailDoUsuário
  'password': passwordDoUsuário
}
```

Por motivos de segurança, essa requisição não retorna a razão do erro se as credenciais não estiverem corretas. Porém, no fluxo em que tudo deu certo, a resposta é:

```
{
    "data": {
        "id": "10",
        "type": "users",
        "attributes": {
            "first-name": "Angela",
            "birth": "1990-03-30",
            "last-login-at": "2018-12-26",
            "is-sysadmin": true,
            "is-medic": true,
            "logs": [],
            "token": "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMCwiZXhwIjoxNTQ1ODM0NjcxfQ.RtCE_LEOaHev9NyLC7_ab6205QgdrKvippRp6ReWY1s"
        }
    }
}
```

Como notado acima, o _endpoint_ retorna as informações do usuário que podem ser armazenadas para realizar outras requisições. Porém, em _endpoints_ que necessitam de autenticação, deve haver um _header_ na requisição com os seguintes dados:

```
{
  "Authentication": "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMCwiZXhwIjoxNTQ1ODM0NjcxfQ.RtCE_LEOaHev9NyLC7_ab6205QgdrKvippRp6ReWY1s"
  "Content-Type": "Content-Type"
}

```

Sendo que o campo "Authentication" deve ter o valor do token enviado em uma tentativa de _login_ bem sucedida.

Esse _header_ permite que a aplicação encontre o seu usuário e valide as ações que estão sendo pedidas.

### Processo de Requisição

Toda requisição, ao chegar na API, passa por um processamento prévio antes do pedido contido na requisição. São duas etapas de processamento, na seguinte ordem:

#### Autenticação

Nessa etapa, o usuário está sendo identificado pelo sistema, caso o _endpoint_ necessite (a maioria necessita). Ocorre aqui as seguintes validações:
* Há um token de autenticação? Se não e o _endpoint_ necessita de autenticação, retorna um erro;
* O token é válido? Se não, retorna um erro;
* O token é válido porém está expirado? Se sim, retorna um erro;

Também aqui é realizado a tentativa de _login_ por parte do usuário, ocorrendo a seguinte validação:
* O usuário inseriu as credenciais corretamente? Se sim, criar um token e retorná-lo junto com as informações de usuário de _login_;
* O usuário inseriu o e-mail corretamente mas errou a senha? Verifica se o usuário excedeu o limite máximo de tentativas. Se excedeu, o usuário é bloqueado;

#### Permissão

Nesta segunda etapa, é verificado se o usuário pode realizar a requisição que deseja. Isso é importante para que um usuário, por exemplo, não possa ser capaz de acessar as informações de reações adversas e alérgicas de outro usuário.

Para isso, segue-se uma diretiva: um usuário não pode acessar informações relacionadas a usuário a não ser que seja sobre ele mesmo. Se um usuário está tentando acessar informações que não pertencem a ele, o sistema verifica se:

* O usuário é um agente de saúde e a requisição que ele deseja está permitida a um agente de saúde;
* O usuário é um administrador do sistema e a requisição que ele deseja está permitida a um administrador;

A um agente de saúde, é permitido realizar as seguintes ações envolvendo outro usuários:
* Criar uma interação relacionada à um usuário;
* Verificar as interações de um usuário;
* Verificar as informações de cadastro de um usuário;

A um administrador do sistema, é permitido realizar as seguintes ações envolvendo outros usuários:
* Atualizar algumas informações de um usuário (se ele deve trocar a senha ou desbloquear um usuário);
* Verificar as informações de cadastro de um usuário;

### Leituras Recomendadas

* [Modelagem do sistema]()
* _Endpoints_:
  * [Users](endpoints/users.md)
  * [Interactions](endpoints/interactions.md)
  * [Licenses](endpoints/licenses.md)
  * [Medicines](endpoints/medicines.md)
  * [Substances](endpoints/substances.md)
  * [Tasks](endpoints/tasks.md)
  * [Hospitals](endpoints/hospitals.md)
  * [Settings](endpoints/settings.md)
  * [Notifications](endpoints/notifications.md)
