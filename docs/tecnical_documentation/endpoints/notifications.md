## Notifications

### 'users/:user_id/notifications'

* **Verbo HTTP**: GET
* **Necessita de autenticação**: Sim

Retorna todas as notificações do usuário identificado por user_id. Ou seja, esse _endpoint_ fornece uma lista com as pessoas que pesquisaram as suas interações, alérgicas ou medicamentosas.
