## Licenses

### '/licenses/'

* **Verbo HTTP**: POST
* **Necessita de autenticação**: Sim

Cria uma licença para o usuário. Essa licença criará, internamente, uma solicitação para o diretor do hospital identificado pelo CNPJ desta requisição.

```
{
  license: {
    license_type: 0 ou 1, sendo 0 um CRM e 1 um COREN,
    number: Registro do CRM ou COREN com máximo de 20 caracteres. Deve ser único,
    state: Estado da licença, escrito por extenso e com letra maiúscula no início de cada palavra
  },
  cnpj: CNPJ do hospital que o usuário está associado.
}
```

### '/users/:user_id/license'

* **Verbo HTTP**: GET
* **Necessita de autenticação**: Sim

Retorna a licença com o id fornecido.
