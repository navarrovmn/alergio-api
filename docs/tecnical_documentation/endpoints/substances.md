## Substances

### '/substances'

* **Verbo HTTP**: GET
* **Necessita de autenticação**: Sim

Retorna os princípios ativos cadastrados no sistema.

Possui um parâmetro opcional chamado **substance_name** que, se fornecido, filtrará os princípios ativos para aqueles que se adequarem ao parâmetro.

### '/substances/:id'

* **Verbo HTTP**: PATCH
* **Necessita de autenticação**: Sim
* **Necessita ser administrador do sistema**

Atualiza o nome de um princípio ativo.

```
substance: {
  name: Novo nome. Deve ser único na aplicação.
}
```

### '/substances/:id'

* **Verbo HTTP**: DELETE
* **Necessita de autenticação**: Sim
* **Necessita ser administrador do sistema**

Deleta um princípio ativo da aplicação.

### '/substances/:id'

* **Verbo HTTP**: GET
* **Necessita de autenticação**: Sim

Retorna o princípio ativo com o id fornecido.
