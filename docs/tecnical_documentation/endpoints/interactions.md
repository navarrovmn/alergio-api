## Interactions

### '/users/:user_id/interactions'

* **Verbo HTTP**: GET
* **Necessita de autenticação**: Sim

Retorna as reações adversas e alérgicas de um usuário, bem como os medicamentos que está consumindo.

Se a consulta não estiver sendo realizada pelo próprio usuário, uma notificação será criada para informá-lo de que seus dados estão sendo acessados.

O parâmetro **user_id** pode referir-se tanto ao id no sistema do usuário quanto ao CPF do usuário que está sendo pesquisado.

### '/users/:user_id/interactions'

* **Verbo HTTP**: POST
* **Necessita de autenticação**: Sim

Cria uma interação do usuário identificado por user_id.

```
{
  interaction: {
    user_id: usuário da interação,
    substance_id: princípio ativo da interação,
    adverse: true se for uma reação adversa ou alérgica e false se for consumo de um medicamento,
    description: descrição da necessidade do medicamento ou dos efeitos colaterais da reação adversa,
  }
}
```


### '/users/:user_id/interactions/:id'

* **Verbo HTTP**: PATCH
* **Necessita de autenticação**: Sim

Atualiza as informações de uma interação, sendo que o único parâmetro que pode ser alterado é a descrição.

```
{
  interaction: {
    description: descrever o que mudou nessa interação
  }
}
```

### '/users/:user_id/interactions/:id'

* **Verbo HTTP**: DELETE
* **Necessita de autenticação**: Sim

Deleta a interação identificada por id.
