## Tasks

### ':user_id/tasks'

* **Verbo HTTP**: GET
* **Necessita de autenticação**: Sim

Retorna as _tasks_ não avaliadas de um diretor identificado pelo user_id.


### '/tasks/:id'

* **Verbo HTTP**: PATCH
* **Necessita de autenticação**: Sim

_Endpoint_ utilizado para aceitar uma solicitação ou recusá-la. Aceita somente um parâmetro:

```
{
  task: {
    accepted: true ou false
  }
}
```

Ao atualizar como 'true', a solicitação é aceita e a licença associada à solicitação é validada. Dessa forma, o usuário que possui a licença pode agir como médico dentro do sistema.

Ao atualizar como 'false', a licença não é validada e o usuário não pode agir como médico. Pode, porém, criar outra licença, que deverá passar pelo mesmo processo.
