## Hospitals

### '/hospitals'

* **Verbo HTTP**: GET
* **Necessita de autenticação**: Sim

Retorna os hospitais cadastrados na aplicação.

### '/hospitals'

* **Verbo HTTP**: POST
* **Necessita de autenticação**: Sim
* **Necessita ser administrador do sistema**

Cria um hospital na aplicação, ao mesmo que tempo que encontra o usuário que será o diretor do mesmo e o eleva a agente de saúde.

Para isso, o usuário que será diretor do hospital deve estar previamente cadastrado na aplicação.

```
{
  hospital: {
    name: nome do hospital,
    cnpj: CNPJ do hospital (deve ser válido),
    state: estado do hospital por extenso e com letras iniciais maiúsculas,
    license_attributes: [
      {
        license_type: verificar na página de licença,
        number: verificar na página de licença,
        state: estado da licença por extenso e com letras iniciais,
        approved: true para o usuŕio tornar-se um diretor,
      }
    ]
  },
  cpf: CPF do usuário que será o diretor do hospital
}
```

### '/hospital/:id'

* **Verbo HTTP**: PATCH
* **Necessita de autenticação**: Sim
* **Necessita ser o diretor do hospital**

Atualiza as informações de um hospital. Apenas os parâmetros abaixo são permitidos para mudança.

```
{
  hospital: {
    name: novo nome,
    user_id: novo diretor
  }
}
```

O id pode ser tanto o id dentro da aplicação quanto o CNPJ do hospital cadastrado.

### '/hospital/:id'

* **Verbo HTTP**: GET
* **Necessita de autenticação**: Sim
* **Necessita ser o diretor do hospital**

Retorna o hospital com o id fornecido. O id pode ser tanto o id dentro da aplicação quanto o CNPJ do hospital cadastrado.
