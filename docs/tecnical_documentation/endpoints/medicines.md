## Medicines

### '/medicines'

* **Verbo HTTP**: GET
* **Necessita de autenticação**: Sim

Retorna todos os medicamentos cadastrados na aplicação.

Possui um parâmetro opcional chamado **medicine_name** que, se fornecido, filtrará os remédios para aqueles que se adequarem ao parâmetro.

### '/medicines'

* **Verbo HTTP**: POST
* **Necessita de autenticação**: Sim
* **Necessita ser administrador do sistema**

Cria um medicamento na aplicação. É importante salientar que esse _endpoint_ cria um medicamento e ao mesmo tempo seus princípios ativos, visto que não é coerente criar um princípio ativo sem medicamentos no sistema.

```
{
  medicine: {
    name: Nome do remédio,
    register: Primeiros 9 dígitos do registro do medicamento na ANVISA. Deve ser único,
    substances_attributes: [
      { name: Nome do princípio ativo, deve ser único }
    ]
  }
}
```

### '/medicines/:id'

* **Verbo HTTP**: GET
* **Necessita de autenticação**: Sim

Retorna o remédio com o id fornecido.

### '/medicines/:id'

* **Verbo HTTP**: PATCH
* **Necessita de autenticação**: Sim
* **Necessita ser administrador do sistema**

Atualiza as informações de um remédio cadastrado no sistema.

### '/medicines/:id'

* **Verbo HTTP**: DELETE
* **Necessita de autenticação**: Sim
* **Necessita ser administrador do sistema**

Deleta o remédio com o id fornecido.
