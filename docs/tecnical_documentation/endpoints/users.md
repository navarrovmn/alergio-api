## User

## '/users/'

* **Verbo HTTP**: POST
* **Necessita de autenticação**: Não

_Endpoint_ responsável pela criação de um novo usuário com os seguintes parâmetros:

```
{
  first_name: apenas o primeiro nome,
  cpf: cpf válido da pessoa a ser cadastrada,
  email: e-mail com formatação válida, ex: fulano@gmail.com,
  birth: data de nascimento da pessoa no formato 'ano-mês-dia',
  password: senha que deve conter no mínimo 8 caracteres (no mínimo 1 numérico e 1 alfabético),
  password_confirmation: confirmação da senha
}
```

## '/users/:id'

* **Verbo HTTP**: GET
* **Necessita de autenticação**: Sim

Retorna as informações de um usuário identificado por id.

Nesse _endpoint_, o id pode referir-se tanto ao id interno na aplicação quanto ao CPF da pessoa.

## '/users/:id'

* **Verbo HTTP**: PATCH
* **Necessita de autenticação**: Sim

Faz a atualização dos dados de um usuário. Esse _endpoint_ é delicado, pois o usuário pode atualizar suas próprias informações ou um administrador do sistema pode atualizar alguns atributos do usuário para alterar o comportamento do mesmo na aplicação.

Caso o usuário seja um administrador que quer alterar outro usuário, os parâmetros permitidos são:

```
{
  must_change_password: true é o único valor aceitável para esse campo,
  blocked: false é o único valor aceitável para esse campo
}
```

Caso o usuário deseja alterar suas próprias informações, os parâmetros permitidos são:

```
{
  first_name: caso o nome da pessoa seja alterado, o novo nome deve ser inserido,
  email: novo valor de e-mail,
  password: nova senha que deve ser diferente da previamente cadastrada,
  password_confirmation: confirmação da nova senha
}
```

Nesse _endpoint_, o id pode referir-se tanto ao id interno na aplicação quanto ao CPF da pessoa.

## '/users/:id/medicines/:medicine_id'

* **Verbo HTTP**: GET
* **Necessita de autenticação**: Sim

Retorna se o remédio identificado por medicine_id pode ser consumido pelo usuário identificado por id, dada suas reações adversas e alérgicas cadastradas no sistema.

Nesse _endpoint_, o id pode referir-se tanto ao id interno na aplicação quanto ao CPF da pessoa.
