## Settings

### '/settings'

* **Verbo HTTP**: GET
* **Necessita de autenticação**: Sim
* **Necessita ser administrador do sistema**

Retorna as configurações do sistema, sendo elas:
* time_to_change_password: Tempo, em meses, que o usuário deve trocar sua senha;
* maximum_login_tries: Número de tentativas de _login_ que um usuário pode realizar sem ser bloqueado;
* time_to_invalidate_token: Tempo, em minutos, em que um _token_ é válido para realizar requisições. Após esse tempo, o usuário deve realizar _login_ novamente.

### '/settings'

* **Verbo HTTP**: PATCH
* **Necessita de autenticação**: Sim
* **Necessita ser administrador do sistema**

Atualiza as configurações de ambiente.

```
  setting: {
    time_to_change_password: Um valor maior que 6
    maximum_login_tries: Um valor que não exceda 10
    time_to_invalidate_token: Um valor entre 5 e 20
  }
```
