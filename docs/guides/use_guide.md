## Instruções de Uso

Allergio é um projeto de _software_ livre. Isso significa que qualquer pessoa pode criar sua própria instância da aplicação ou contribuir para o desenvolvimento da aplicação.

Para uso em desenvolvimento, pode-se clonar a aplicação e rodar os seguintes comandos (assumindo que você possui Ruby instalado na sua máquina, bem como bundler):

```
$ bundle install
$ rails db:migrate
$ rails s
```

Isso fará o servidor iniciar localmente na porta 3000. Deve ser possível, então, acessar o navegador e utilizar a aplicação.

Por se tratar de uma API, é altamente recomendável a interação com a mesma através do uso de _softwares_ para esse fim, como Postman.

Caso você deseja desenvolver utilizando _Docker_, há uma _branch_ de mesmo nome no projeto configurada com um docker-compose de desenvolvimento.

## Instalação e Configuração

Caso você não esteja utilizando a aplicação para desenvolvimento, deve-se tomar algumas precauções de forma a atender medidas de seguranças:

* Na sua configuração de _deploy_, certifique-se que a comunicação com seu servidor utiliza protocolo HTTPS. Isso evita que pessoas possam roubar as informações trafegando entre os clientes e seu servidor de diversas formas possíveis;
* Certifique-se que há uma rotina de _backup_ para o banco de dados na sua configuração de _deploy_;

Na configuração de _deploy_ desenvolvida para esta aplicação, utilizou-se o Amazon Web Services (AWS) Beanstalk, um serviço para implantação de aplicativos _web_.

O Beanstalk oferece uma forma relativamente fácil de implantar o serviço, fornecendo também um serviço de certificação digital, para que a comunicação ocorra de forma segura entre cliente e servidor.

Indo mais além, pode-se utilizar o AWS Relational Database Service (RDS), que opera as rotinas de _backup_ para que os dados nunca sejam perdidos.

Mais informações sobre o AWS Beanstalk utilizado nesse projeto podem ser encontradas [aqui](https://aws.amazon.com/pt/elasticbeanstalk/). É importante mencionar que essa é a configuração que o desenvolvedor do Allergio escolheu utilizar, não sendo via de regra.

A única necessidade é que haja comunicação segura e _backup_ dos dados e isso pode ser alcançado de diversas maneiras diferentes.

Para efetivamente utilizar a aplicação, deve-se entrar no _rails console_ (no ambiente que você deseja. Talvez seja necessário entrar via SSH no servidor da sua aplicação) e criar um usuário _sysadmin_:

```
$ rails c
$ User.create(
    cpf: "02364598523",
    email: "victor@gmail.com".
    first_name: "Victor",
    birth: "1996-03-31",
    is_sysadmin: true,
    password: "abcd1234",
    password_confirmation: "abcd1234"
  )
```

Esse foi apenas um exemplo. Obviamente, insira suas informações. Depois de verificar que o usuário foi criado corretamente, você está apto a utilizar a aplicação de forma efetiva. É recomendada a leitura da documentação técnica para que esteja mais claro o que pode ser feito.

## Notas Finais

O _Allergio_ permite uma gama de objetivos de desenvolvimento. Por ser uma API centrada no uso hospitalar, um uso óbvio é o de que aplicações médicas já existentes possam utilizar as informações contidas no seu servidor.

Porém, para o cadastro dessas informações na aplicação, é necessário um _frontend_. Isso também é uma alternativa de desenvolvimento com o projeto, ou seja, desenvolver um _frontend_ intuitivo que faça uso da documentação técnica fornecida para garantir ao usuário final uma experiência de uso agradável.

Caso esteja desenvolvendo um _frontend_ ou esteja interessado em propor uma nova alternativa de desenvolvimento que utilize esse projeto, solicito que entre em contato no e-mail victor.matias.navarro@gmail.com
