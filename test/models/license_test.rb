require 'test_helper'

class LicenseTest < ActiveSupport::TestCase
  test "should create license" do
    carer = create_user
    license = License.create(license_type: 1, number: "000000", approved: true, state: "Distrito Federal", user_id: carer.id)

    assert license.valid?
  end

  test "fields must be valid" do
    license = License.create()
    fields = [:license_type, :number, :state]

    assert_equal false, license.valid?
    assert_equal false, fields_valid?(fields, license.errors)
  end

  test "type of license should be 0 or 1" do
    license = License.create(license_type: 2)
    another_license = License.create(license_type: -1)
    valid_license_type = License.create(license_type: 0)
    another_valid_license_type = License.create(license_type: 1)

    assert_equal false, license.valid?
    assert_equal false, another_license.valid?
    assert_equal true, !license.errors.details[:license_type].blank?
    assert_equal true, !another_license.errors.details[:license_type].blank?
    assert_equal true, valid_license_type.errors.details[:license_type].blank?
    assert_equal true, another_valid_license_type.errors.details[:license_type].blank?
  end

  test "license number should not be more than 20 digits" do
    license = License.create(number: "000000000000000000000")

    assert_equal false, license.valid?
    assert_equal true, !license.errors.details[:number].blank?
  end

  test "should not create license with invalid state" do
    license = License.create(state: "Dist Fed")

    assert_equal false, license.valid?
    assert_equal true, !license.errors.details[:state].blank?
  end
end
