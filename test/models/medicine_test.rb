require 'test_helper'

class MedicineTest < ActiveSupport::TestCase
  test "fields must be valid" do
    medicine = Medicine.create(name: "", register: "145")

    assert_equal false, medicine.valid?
    assert_equal false, medicine.errors.messages[:register].empty?
    assert_equal false, medicine.errors.messages[:name].empty?
  end

  test "should not create two medicines with same register" do
    medicine = Medicine.create(name: "Tylenol", register: "123456789")
    another_medicine = Medicine.create(name: "Advil", register: "123456789")

    assert_equal 1, Medicine.all.count
    assert_equal false, another_medicine.valid?
    assert_equal false, another_medicine.errors.messages[:register].empty?
  end

  test "should not create medicine if register is different than 9 digits" do
    medicine = Medicine.create(name: "Tylenol", register: "12345")
    another_medicine = Medicine.create(name: "Tylenol", register: "12345678910")

    assert_equal false, medicine.valid?
    assert_equal false, another_medicine.valid?
    assert_equal false, medicine.errors.details[:register].empty?
    assert_equal false, another_medicine.errors.details[:register].empty?
  end
end
