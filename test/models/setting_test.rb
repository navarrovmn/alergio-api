require 'test_helper'

class SettingTest < ActiveSupport::TestCase
  test "should not exist more than one setting" do
    first_instance = Setting.instance
    second_instance = Setting.create

    assert_equal 1, Setting.all.count
  end

  test "time to change password should not be less than 6 months" do
    Setting.instance.time_to_change_password = 2
    Setting.instance.save

    assert_equal false, Setting.instance.valid?
    assert_equal false, Setting.instance.errors.details[:time_to_change_password].empty?
  end

  test "maximum login_tries should not exceed 10" do
    Setting.instance.maximum_login_tries = 11
    Setting.instance.save

    assert_equal false, Setting.instance.valid?
    assert_equal false, Setting.instance.errors.details[:maximum_login_tries].empty?
  end

  test "time to invalidate token must be equal or greater than five or less or equal to 20" do
    Setting.instance.time_to_invalidate_token = 4
    Setting.instance.save

    assert_equal false, Setting.instance.valid?
    assert_equal false, Setting.instance.errors.details[:time_to_invalidate_token].empty?

    Setting.instance.time_to_invalidate_token = 21
    Setting.instance.save

    assert_equal false, Setting.instance.valid?
    assert_equal false, Setting.instance.errors.details[:time_to_invalidate_token].empty?
  end
end
