require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "should register a valid person" do
    user = create_user

    assert_equal 1, User.all.count
  end

  test "email should be valid" do
    user = create_user options={email: "vicgmailcom"}

    assert_equal false, user.valid?
    assert_equal 0, User.all.count
    assert_equal "is invalid", user.errors.messages[:email].first
  end

  test "must not register an email with an existing email" do
    user = create_user options={email: "victor@gmail.com"}
    another_user = create_user options={email: "victor@gmail.com"}

    assert_equal 1, User.all.count
    assert_equal false, another_user.valid?
    assert_equal "has already been taken",
      another_user.errors.messages[:email].first
  end

  test "cpf must have 11 digits" do
    user = create_user options={cpf: "9884037"}

    assert_equal 0, User.all.count
    assert_equal false, user.valid?
    assert_equal "is the wrong length (should be 11 characters)",
      user.errors.messages[:cpf].first
  end

  test "must not register an user with an existing cpf" do
    user = create_user
    another_user = create_user options={cpf: user.cpf}

    assert_equal 1, User.all.count
    assert_equal false, another_user.valid?
    assert_equal "has already been taken",
      another_user.errors.messages[:cpf].first
  end

  test "must not have a 1 character name" do
    user = create_user options={first_name: "V"}

    assert_equal 0, User.all.count
    assert_equal false, user.valid?
    assert_equal "is too short (minimum is 2 characters)",
      user.errors.messages[:first_name].first
  end

  test "must not register an user with invalid cpf" do
    user = create_user options={cpf: "122121212"}

    assert_equal 0, User.all.count
    assert_equal false, user.valid?
    assert_equal false, user.errors.messages[:cpf].empty?
  end

  test "must not register an user with multiple names" do
    user = create_user options={first_name: "Victor Navarro"}

    assert_equal 0, User.all.count
    assert_equal false, user.valid?
    assert_equal false, user.errors.messages[:first_name].empty?
  end

  test "must not register user if has no password" do
    user = create_user options={password: "", password_confirmation: ""}

    assert_equal 0, User.all.count
    assert_equal false, user.valid?
  end

  test "must not register user if password does not have digits and alphabetic chars" do
    user = create_user options={password: "abcdefgh", password_confirmation: "abcdefgh"}

    assert_equal 0, User.all.count
    assert_equal false, user.valid?

    user.password="12345678"
    user.password_confirmation="12345678"
    user.save

    assert_equal 0, User.all.count
    assert_equal false, user.valid?
  end
end
