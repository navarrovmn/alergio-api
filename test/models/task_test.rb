require 'test_helper'

class TaskTest < ActiveSupport::TestCase
  setup do
    @user = create_user
    @admin = create_user(options={is_sysadmin: true})
    @license = License.create(
      license_type: 1,
      number: "12345678",
      state: "Distrito Federal",
      user_id: @user.id
    )
    @task = Task.create(user_id: @admin.id, license_id: @license.id)
  end

  test "cannot create task without user and license" do
    another_task = Task.create()

    assert_equal false, another_task.valid?
    assert_equal false, another_task.errors.messages[:user].empty?
    assert_equal false, another_task.errors.messages[:license].empty?
  end

  test "if a task is accepted, a user is a medic" do
    @task.close(true)

    assert_equal true, @user.is_medic?
    assert_equal true, @task.accepted
    assert_equal false, @admin.is_medic?
  end

  test "if a task is rejected, a user should not be seen as a medic" do
    @task.close(false)

    assert_equal false, @user.is_medic?
    assert_equal false, @task.accepted
    assert_equal false, @admin.is_medic?
  end
end
