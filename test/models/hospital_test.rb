require 'test_helper'

class HospitalTest < ActiveSupport::TestCase
  test "should create hospital" do
    carer = create_user
    License.create(license_type: 1, number: "000000", approved: true, state: "Distrito Federal", user_id: carer.id)

    hospital = Hospital.create(name: "Hospital Light", cnpj: CNPJ.generate, state: "Paraná", user_id: carer.id)

    assert hospital.valid?
  end

  test "fields should be valid" do
    fields = [:name, :cnpj]
    hospital = Hospital.create()

    assert_equal false, fields_valid?(fields, hospital.errors)
  end

  test "should not create hospitals without user" do
    hospital = Hospital.create(name: "Hospital Light", cnpj: CNPJ.generate, state: "Distrito Federal")

    assert_equal false, hospital.valid?
    assert_equal true, !hospital.errors.details[:user].blank?
  end

  test "should not create two hospitals with same cnpj" do
    carer = create_user
    License.create(license_type: 1, number: "000000", approved: true, state: "Distrito Federal", user_id: carer.id)

    hospital = Hospital.create(name: "Hospital Light", cnpj: CNPJ.generate, state: "Distrito Federal", user_id: carer.id)
    another_hospital = Hospital.create(name: "Another Hospital", cnpj: hospital.cnpj, state: "Distrito Federal", user_id: carer.id)

    assert_equal false, another_hospital.valid?
    assert_equal true, !another_hospital.errors.details[:cnpj].blank?
  end

  test "should not create hospital with invalid state" do
    hospital = Hospital.create(name: "Hospital Light", cnpj: CNPJ.generate, state: "Distr Fed")

    assert_equal false, hospital.valid?
    assert_equal true, !hospital.errors.details[:state].blank?
  end
end
