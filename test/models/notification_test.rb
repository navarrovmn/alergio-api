require 'test_helper'

class NotificationTest < ActiveSupport::TestCase
  test "all fields must not be blank" do
    fields = [:carer, :patient, :seen_at]
    notification = Notification.create()

    assert_equal false, notification.valid?
    assert_equal 3, notification.errors.messages.count
    notification.errors.messages.each do |key, errors|
      assert fields.include? key
      assert errors.include? "can't be blank"
    end
  end
end
