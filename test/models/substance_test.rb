require 'test_helper'

class SubstanceTest < ActiveSupport::TestCase
  test "fields should be valid" do
    substance = Substance.create(name: "")

    assert_equal false, substance.valid?
    assert_equal false, substance.errors.messages[:name].empty?
  end

  test "should not create two substances with the same name" do
    substance = Substance.create(name: "Amoxilina")
    another_substance = Substance.create(name: "Amoxilina")

    assert substance.valid?
    assert_equal false, another_substance.valid?
    assert_equal false, another_substance.errors.details[:name].empty?
  end
end
