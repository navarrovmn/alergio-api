require 'simplecov'
require 'faker'
require 'json'
SimpleCov.start

ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'

class ActiveSupport::TestCase
  fixtures :all

  def login(email, password)
    post sessions_url, params: { email: email, password: password }

    body = JSON.parse(response.body)
    body["data"]["attributes"]["token"] if body["data"]
  end

  def fields_valid?(fields, errors={})
    fields.each do |field|
      if !errors.messages[field].empty?
        return false
      end
    end

    true
  end

  def create_user(options={}, save=true)
    user = User.new(
      first_name: Faker::Name.first_name,
      email: Faker::Internet.email,
      cpf: CPF.generate,
      birth: Faker::Date.between(35.years.ago, 18.years.ago),
      is_sysadmin: false,
      last_changed_password_at: Time.now,
      password: "1234abcd",
      password_confirmation: "1234abcd"
    )

    options.each do |opt, val|
      if user.respond_to? opt.to_s
        method = opt.to_s + "="
        user.send method.to_sym, val
      end
    end

    if save
      user.save
    end

    user
  end

  def make_user_carer(user, hospital_owner)
    license = License.create(
      user_id: user.id,
      license_type: 1,
      number: "000000000",
      state: "Distrito Federal",
    )

    task = Task.create(license_id: license.id, user_id: hospital_owner.id)
    task.close(:accept)
  end
end
