require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = create_user
  end

  test "should create user" do
    post users_url, params: {
      birth: @user.birth,
      cpf: CPF.generate,
      first_name: Faker::Name.first_name,
      email: Faker::Internet.email,
      password: "1234abcd",
      password_confirmation: "1234abcd"
    }, as: :json

    assert_equal 2, User.count
    assert_response 201
  end

  test "should update user if sysadmin as to force password change or unblock a user" do
    @sysadmin = create_user(options = { is_sysadmin: true })
    token = login(@sysadmin.email, "1234abcd")

    patch user_url(@user), params: {
      must_change_password: true,
      blocked: false
    }, headers: { "Authentication": token }, as: :json
    @user.reload

    assert_response :success
    assert @user.must_change_password
  end

  test "should not let sysadmin block a user" do
    @sysadmin = create_user(options = { is_sysadmin: true })
    token = login(@sysadmin.email, "1234abcd")

    patch user_url(@user), params: {
      blocked: true
    }, headers: { "Authentication": token }, as: :json
    @user.reload

    assert_response :unprocessable_entity
    assert_equal false, @user.blocked
  end

  test "should not let sysadmin free user from changing password" do
    @sysadmin = create_user(options = { is_sysadmin: true })
    token = login(@sysadmin.email, "1234abcd")

    @user.update_attribute(:must_change_password, true)
    patch user_url(@user), params: {
      must_change_password: false
    }, headers: { "Authentication": token }, as: :json
    @user.reload

    assert_response :unprocessable_entity
    assert @user.must_change_password
  end

  test "should not let sysadmin change any other attribute" do
    @sysadmin = create_user(options = { is_sysadmin: true })
    token = login(@sysadmin.email, "1234abcd")
    new_cpf = CPF.generate

    patch user_url(@user), params: {
      first_name: "Jose",
      cpf: new_cpf,
      password: "another_password1",
      password_confirmation: "another_password1",
      must_change_password: true
    }, headers: { "Authentication": token }, as: :json
    @user.reload

    assert_response :success
    assert @user.must_change_password
    assert_not_equal "Jose", @user.first_name
    assert_not_equal new_cpf, @user.cpf
    assert_equal false, @user.authenticate("another_password1")
  end

  test "should update user" do
    token = login @user.email, "1234abcd"

    patch user_url(@user), params: {
      first_name: "Jose",
      password: "another_password1",
      password_confirmation: "another_password1"
    }, headers: { "Authentication": token }, as: :json

    @user.reload

    assert_equal "Jose", @user.first_name
    assert @user.authenticate "another_password1"
    assert_response 200
  end

  test "should not update user if changes to same password" do
    token = login @user.email, "1234abcd"

    patch user_url(@user), params: {
      first_name: "Jose",
      password: "1234abcd",
      password_confirmation: "1234abcd"
    }, headers: { "Authentication": token }, as: :json

    @user.reload

    assert_response 422
    assert_not_equal "Jose", @user.first_name
  end

  test "should create notification when a user is shown to a carer" do
    @another_user = create_user
    @license = License.create(
      license_type: 1,
      number: "11111111",
      approved: true,
      state: "Distrito Federal",
      user_id: @another_user.id
    )
    token = login @another_user.email, "1234abcd"

    get user_interactions_url(@user), headers: { "Authentication" => token }, as: :json
    notification = Notification.first

    assert_equal 1, Notification.all.count
    assert_equal @another_user.id, notification.carer_id
    assert_equal @user.id, notification.patient_id
  end

  test "a user cannot access another users urls" do
    @another_user = create_user
    token = login @another_user.email, "1234abcd"

    get user_url(@user), headers: { "Authentication" => token }, as: :json

    assert_response :unauthorized
  end

  test "a user should not be able to access other urls when he must change password" do
    @another_user = create_user(options = { is_sysadmin: true })
    token = login(@another_user.email, "1234abcd")

    patch user_url(@user), params: {
      must_change_password: true
    }, headers: { "Authentication": token }, as: :json

    token = login(@user.email, "1234abcd")

    get user_url(@user), headers: { "Authentication" => token }, as: :json

    assert_response :forbidden
  end

  test "a user must be able to access pages if he changes his password after being forced" do
    @another_user = create_user(options = { is_sysadmin: true })
    token = login(@another_user.email, "1234abcd")

    patch user_url(@user), params: {
      must_change_password: true
    }, headers: { "Authentication": token }, as: :json

    token = login(@user.email, "1234abcd")

    patch user_url(@user), params: {
      password: "another_password1",
      password_confirmation: "another_password1"
    }, headers: { "Authentication": token }, as: :json

    assert_response :success

    @user.reload

    token = login(@user.email, "another_password1")

    get user_url(@user), headers: { "Authentication" => token }, as: :json

    assert_response :success
  end

  test "should not permit access if user has not changed password for a set amount of time" do
    token = login(@user.email, "1234abcd")

    get user_url(@user), headers: { "Authentication" => token }, as: :json

    assert_response :success

    travel(8.months)

    token = login(@user.email, "1234abcd")

    get user_url(@user), headers: { "Authentication" => token }, as: :json

    @user.reload

    assert_response :forbidden
    assert_equal true, @user.must_change_password
  end

  test "user should be blocked if he tries to access application several times failing" do
    (Setting.instance.maximum_login_tries + 1).times do |login_attempt|
      login(@user.email, "wrongpassword")
    end

    @user.reload

    assert @user.blocked
  end

  test "user should not login if he is blocked" do
    (Setting.instance.maximum_login_tries + 1).times do |login_attempt|
      login(@user.email, "wrongpassword")
    end

    @user.reload

    token = login(@user.email, "wrongpassword")

    assert_response :forbidden
  end

  test "user should be able to login if he is unblocked" do
    (Setting.instance.maximum_login_tries + 1).times do |login_attempt|
      login(@user.email, "wrongpassword")
    end

    @another_user = create_user(options = { is_sysadmin: true })
    token = login(@another_user.email, "1234abcd")

    patch user_url(@user), params: {
      blocked: false
    }, headers: { "Authentication": token }, as: :json
    @user.reload

    token = login(@user.email, "1234abcd")

    assert_response :success
    assert_equal false, @user.blocked
  end

  test "only a sysadmin should be able to unblock a user" do
    token = login(@user.email, "1234abcd")

    @user.update_attribute(:blocked, true)
    patch user_url(@user), params: {
      blocked: false
    }, headers: { "Authentication": token }, as: :json
    @user.reload

    assert_response :success
    assert_equal true, @user.blocked
  end

  test "old logs should be erased as not to block user" do
    Setting.instance.maximum_login_tries.times do |login_attempt|
      login(@user.email, "wrongpassword")
    end

    assert_equal Setting.instance.maximum_login_tries, Log.where(user_id: @user.id).count

    @token = login(@user.email, "1234abcd")

    assert_equal 0, Log.where(user_id: @user.id).count
  end
end
