require 'test_helper'

class LicensesControllerTest < ActionDispatch::IntegrationTest
    setup do
        @user = create_user
        @hospital_owner = create_user
        make_user_carer(@hospital_owner, @user)
        @hospital = Hospital.create(name: "Hospital Light", cnpj: CNPJ.generate, user_id: @hospital_owner.id, state: "Amazonas")

        @token = login(@user.email, "1234abcd")
    end

    test "should create license, given CNPJ" do
        license_params = {
            license: {
                user_id: @user.id,
                license_type: 1,
                number: "9999999",
                state: "Amazonas"
            },
            cnpj: @hospital.cnpj
        }

        puts @hospital.errors.full_messages

        post user_licenses_url(@user),
        params: license_params,
        headers: { "Authentication": @token },
        as: :json

        assert_response :created
    end

    test "should not create license without CNPJ" do
        license_params = {
            license: {
                user_id: @user.id,
                license_type: 1,
                number: "9999999",
            }
        }

        post user_licenses_url(@user),
        params: license_params,
        headers: { "Authentication": @token },
        as: :json

        assert_response :bad_request
    end

    test "should not show license to another user other than the owner" do
        get user_licenses_url(@hospital_owner),
        headers: { "Authentication": @token },
        as: :json

        assert_response :unauthorized
    end
end
