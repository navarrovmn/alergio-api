require 'test_helper'

class AuthControllerTest < ActionDispatch::IntegrationTest

  setup do
    @user = create_user
  end

  test "should be able to retrieve token with correct password" do
    login(@user.email, "1234abcd")

    assert response.body.include? "token"
    assert_response :created
  end

  test "should not be able to retrieve token with incorrect password" do
    login(@user.email, "wrong")

    assert response.body.include? "error"
    assert_response :unauthorized
  end

  test "should not access url without token" do
    get user_url(@user), as: :json

    assert_response :unauthorized
  end

  test "should access url with token" do
    token = login(@user.email, "1234abcd")

    get user_url(@user), headers: { "Authentication": token }, as: :json

    assert_response :success
  end

  test "token should not be valid after expiration" do
    token = login(@user.email, "1234abcd")

    travel(21.minutes)

    get user_url(@user), headers: { "Authentication" => token }, as: :json

    assert response.body.include? "Token has expired"
    assert_response :unauthorized
  end

  test "should not access urls with invalid token" do
    get user_url(@user),
    headers: {
      "Authentication": "Surely not valid"
    }, as: :json

    assert response.body.include? "Invalid token"
    assert_response :unauthorized
  end

end
