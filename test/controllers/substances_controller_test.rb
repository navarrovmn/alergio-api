require 'test_helper'

class SubstancesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @substance = Substance.create(name: "Paracetamol")
    @user = create_user
    @token = login(@user.email, "1234abcd")
  end

  test "should get index" do
    get substances_url, headers: { "Authentication": @token }, as: :json

    assert_response :success
  end

  test "should update substance if sysadmin" do
    @user.update_attributes(is_sysadmin: true)
    old_name = @substance.name

    patch substance_url(@substance), params: {
      substance: {
        name: "Oxido de Orni"
      }
    },
    headers: {
      "Authentication": @token
    }, as: :json

    @substance.reload

    assert_equal false, @substance.name == old_name
    assert_response :success
  end

  test "should destroy substance if sysadmin" do
    @user.update_attributes(is_sysadmin: true)

    assert_difference('Substance.count', -1) do
      delete substance_url(@substance), headers: { "Authentication": @token }, as: :json
    end

    assert_response 204
  end

  test "should not update substance if not sysadmin" do
    patch substance_url(@substance), params: {
      substance: {
        name: "Avidre"
      }
    },
    headers: {
      "Authentication": @token
    }, as: :json

    assert_response :unauthorized
  end

  test "should not destroy substance if not sysadmin" do
    delete substance_url(@substance), headers: { "Authentication": @token }, as: :json

    assert_response :unauthorized
  end
end
