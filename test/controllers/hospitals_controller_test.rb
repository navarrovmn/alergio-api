require 'test_helper'

class HospitalsControllerTest < ActionDispatch::IntegrationTest
    setup do
        @sysadmin = create_user({ is_sysadmin: true })
        @user = create_user
    end

    test "only a sysadmin should be able to create a hospital" do
        token = login(@user.email, "1234abcd")
        params = {
            name: "Hospital"
        }

        post hospitals_url,
        params: params,
        headers: { "Authentication": token }, as: :json

        assert_response :unauthorized
        assert_equal 0, Hospital.all.count
    end

    test "should create hospital with license" do
        token = login(@sysadmin.email, "1234abcd")
        params = {
            hospital: {
                name: "Hospital Name",
                cnpj: CNPJ.generate,
                user_id: @user.id,
                state: "Paraná",
                license_attributes: {
                    license_type: 1,
                    number: "123456",
                    state: "Paraná",
                    user_id: @user.id,
                    approved: true
                }
            }
        }

        post hospitals_url,
        params: params,
        headers: { "Authentication": token }, as: :json

        @user.reload

        assert_response :created
        assert_equal 1, Hospital.all.count
        assert @user.is_medic?
    end

    test "should not create a hospital without creating a license for owner" do
        token = login(@sysadmin.email, "1234abcd")
        params = {
            hospital: {
                name: "Hospital Name",
                cnpj: CNPJ.generate,
                user_id: @user.id,
                state: "Paraná",
                license_attributes: {
                    license_type: 1,
                    number: "123456",
                }
            }
        }

        post hospitals_url,
        params: params,
        headers: { "Authentication": token }, as: :json

        assert_response :unprocessable_entity
        assert_equal 0, Hospital.all.count
        assert_equal 0, License.all.count
    end

    test "only hospital director can update its information" do
        token = login(@sysadmin.email, "1234abcd")
        params = {
            hospital: {
                name: "Hospital Name",
                cnpj: CNPJ.generate,
                user_id: @user.id,
                state: "Paraná",
                license_attributes: {
                    license_type: 1,
                    number: "123456",
                    state: "Paraná",
                    user_id: @user.id,
                    approved: true
                }
            }
        }

        post hospitals_url,
        params: params,
        headers: { "Authentication": token }, as: :json

        @user.reload

        patch hospital_url(Hospital.last),
        params: params,
        headers: { "Authentication": token }, as: :json

        assert_response :unauthorized

        update_params = {
            hospital: {
                name: "Another hospital name"
            }
        }

        token = login(@user.email, "1234abcd")

        patch hospital_url(Hospital.last),
        params: update_params,
        headers: { "Authentication": token }, as: :json

         assert_response :success
         assert_equal "Another hospital name", Hospital.last.name
    end

    test "should not update any hospital info other than name or director" do
        token = login(@sysadmin.email, "1234abcd")
        params = {
            hospital: {
                name: "Hospital Name",
                cnpj: CNPJ.generate,
                user_id: @user.id,
                state: "Paraná",
                license_attributes: {
                    license_type: 1,
                    number: "123456",
                    state: "Paraná",
                    user_id: @user.id,
                    approved: true
                }
            }
        }

        post hospitals_url,
        params: params,
        headers: { "Authentication": token }, as: :json

        @user.reload
        new_cnpj = CNPJ.generate

        update_params = {
            hospital: {
                cnpj: new_cnpj
            }
        }

        token = login(@user.email, "1234abcd")

        patch hospital_url(Hospital.last),
        params: update_params,
        headers: { "Authentication": token }, as: :json

        assert Hospital.last.cnpj != new_cnpj
    end
end
