require 'test_helper'

class MedicinesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @medicine = Medicine.create(name: "Tylenol", register: "123456789")
    @user = create_user
    @token = login(@user.email, "1234abcd")
  end

  test "should get index" do
    get medicines_url, headers: { "Authentication": @token }, as: :json

    assert_response :success
  end

  test "should not create medicine if not sysadmin " do
    post medicines_url, params: {
      medicine: {
        name: "Advil",
        register: "234567890",
        substances_attributes: [
          { name: "Paracetamol" }
        ]
      }
    }, headers: { "Authentication": @token }, as: :json

    assert_equal 1, Medicine.all.count
    assert_response :unauthorized
  end

  test "should create medicine if sysadmin " do
    @user.update_attributes(is_sysadmin: true)

    @user.reload
    post medicines_url, params: {
      medicine: {
        name: "Advil",
        register: "234567890",
        substances_attributes: [
          { name: "Paracetamol" }
        ]
      }
    }, headers: { "Authentication": @token }, as: :json

    assert_equal 2, Medicine.all.count
    assert_equal 1, Medicine.last.substances.count
    assert_equal "Paracetamol", Medicine.last.substances.first.name
    assert_response :created
  end

  test "should create medicine with more than two substances " do
    @user.update_attributes(is_sysadmin: true)

    @user.reload
    post medicines_url, params: {
      medicine: {
        name: "Advil",
        register: "234567890",
        substances_attributes: [
          { name: "Paracetamol" },
          { name: "Parafitalina" }
        ]
      }
    }, headers: { "Authentication": @token }, as: :json

    assert_equal 2, Medicine.all.count
    assert_equal 2, Medicine.last.substances.count
    assert_equal "Paracetamol", Medicine.last.substances.first.name
    assert_equal "Parafitalina", Medicine.last.substances.second.name
    assert_response :created
  end

    test "should not create more than two substances if they are the same " do
    @user.update_attributes(is_sysadmin: true)

    @user.reload
    post medicines_url, params: {
      medicine: {
        name: "Advil",
        register: "234567890",
        substances_attributes: [
          { name: "Paracetamol" },
          { name: "Parafitalina" }
        ]
      }
    }, headers: { "Authentication": @token }, as: :json

    post medicines_url, params: {
      medicine: {
        name: "Tylenol",
        register: "234567891",
        substances_attributes: [
          { name: "Paracetamol" },
          { name: "Parafitalina" }
        ]
      }
    }, headers: { "Authentication": @token }, as: :json


    assert_equal 3, Medicine.all.count
    assert_equal 2, Medicine.last.substances.count
    assert_equal 2, Substance.all.count
    assert_equal "Paracetamol", Medicine.last.substances.first.name
    assert_equal "Parafitalina", Medicine.last.substances.second.name
    assert_response :created
  end

  test "should show medicine" do
    get medicine_url(@medicine), headers: { "Authentication": @token }, as: :json

    assert_response :success
  end

  test "should not update medicine if not sysadmin" do
    patch medicine_url(@medicine), params: {
      medicine: {
        name: "Advil",
        register: "234567890"
      }
    }, headers: { "Authentication": @token }, as: :json

    assert_response :unauthorized
  end

  test "should update medicine if sysadmin" do
    @user.update_attributes(is_sysadmin: true)
    @user.reload
    old_name = @medicine.name


    patch medicine_url(@medicine), params: {
      medicine: {
        name: "Advil",
        register: "234567890"
      }
    }, headers: {
      "Authentication": @token
    }, as: :json

    @medicine.reload

    assert_response 200
    assert @medicine.name != old_name
  end

  test "should not destroy medicine if not sysadmin" do
    delete medicine_url(@medicine), headers: { "Authentication": @token }, as: :json

    assert_response :unauthorized
  end

  test "should destroy medicine if sysadmin" do
    @user.update_attributes(is_sysadmin: true)
    @user.reload

    assert_difference('Medicine.count', -1) do
      delete medicine_url(@medicine), headers: { "Authentication": @token }, as: :json
    end

    assert_response 204
  end
end
