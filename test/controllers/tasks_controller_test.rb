require 'test_helper'

class TasksControllerTest < ActionDispatch::IntegrationTest
    setup do
        @user = create_user
        @admin = create_user(options={is_sysadmin: true})
        @license = License.create(
        license_type: 1,
        number: "12345678",
        state: "Distrito Federal",
        user_id: @user.id
        )
        @task = Task.create(user_id: @admin.id, license_id: @license.id)
  end

    test "should not be able to close another mans tasks" do
        @another_user = create_user
        token = login(@another_user.email, "1234abcd")

        patch user_task_url(@user, @task),
        params: { accepted: true },
        headers: { "Authentication": token }, as: :json

        assert_response :unauthorized
        assert_nil @task.accepted
    end

    test "should be able to accept task" do
        token = login(@admin.email, "1234abcd")

        patch user_task_url(@admin, @task),
        params: { accepted: true },
        headers: { "Authentication": token }, as: :json
        @task.reload

        assert_response :success
        assert_equal true, @task.accepted
        assert_equal true, @user.is_medic?
    end

    test "should be able to reject task" do
        token = login(@admin.email, "1234abcd")

        patch user_task_url(@admin, @task),
        params: { accepted: false },
        headers: { "Authentication": token }, as: :json
        @task.reload

        assert_response :success
        assert_equal false, @task.accepted
        assert_equal false, @user.is_medic?
    end

    test "should not close task twice" do
        token = login(@admin.email, "1234abcd")

        patch user_task_url(@admin, @task),
        params: { accepted: true },
        headers: { "Authentication": token }, as: :json
        @task.reload

        assert_response :success

        patch user_task_url(@admin, @task),
        params: { accepted: false },
        headers: { "Authentication": token }, as: :json
        @task.reload

        assert_response :unprocessable_entity
    end
end
