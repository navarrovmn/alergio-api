require 'test_helper'

class InteractionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = create_user
    @carer = create_user
    @substance = Substance.create(name: "Paracetamol")

    @token = login(@user.email, "1234abcd")
    @carer_token = login(@carer.email, "1234abcd")
  end

  test "should get index" do
    get user_interactions_url(@user), headers: { "Authentication": @token }, as: :json

   assert_response :success
  end

  test "should create interaction" do
    interaction = {
      interaction: {
        user_id: @user.id,
        substance_id: @substance.id,
        adverse: true,
        description: "Headache"
      }
    }

    post user_interactions_url(@user),
    params: interaction,
    headers: { "Authentication": @token },
    as: :json

    assert_response 201
  end

  test "should not create interaction while not authenticated" do
    interaction = {
      interaction: {
        user_id: @user.id,
        substance_id: @substance.id,
        adverse: true,
        description: "Headache"
      }
    }

    post user_interactions_url(@user), params: interaction, as: :json

    assert_response :unauthorized
  end

  test "should differentiate between info inserted by carer and user" do
    interaction = {
      interaction: {
        user_id: @user.id,
        substance_id: @substance.id,
        adverse: true,
        description: "Headache"
      }
    }

    post user_interactions_url(@user),
    params: interaction,
    headers: { "Authentication": @token },
    as: :json

    assert_response 201
    assert Interaction.last.inserted_by_self

    another_interaction = {
      interaction: {
        user_id: @user.id,
        substance_id: @substance.id,
        adverse: true,
        description: "Dehidration"
      }
    }

    @hospital_owner = create_user
    make_user_carer(@carer, @hospital_owner)
    @carer.reload
    @another_token = login(@carer.email, "1234abcd")

    post user_interactions_url(@user),
    params: another_interaction,
    headers: { "Authentication": @another_token },
    as: :json

    assert_response 201
    assert_equal false, Interaction.last.inserted_by_self
  end

  test "a normal user should not be able to see another users allergies" do
    @another_user = create_user

    get user_interactions_url(@another_user),
    headers: { "Authentication": @token },
    as: :json

    assert_response :unauthorized
  end

  test "a normal user should not be able to create another user allergy" do
    @another_user = create_user

    interaction = {
      interaction: {
        user_id: @user.id,
        substance_id: @substance.id,
        adverse: true,
        description: "Headache"
      }
    }

    post user_interactions_url(@another_user),
    params: interaction,
    headers: { "Authentication": @token },
    as: :json

    assert_response :unauthorized
  end
end
