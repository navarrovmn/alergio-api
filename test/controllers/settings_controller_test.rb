require 'test_helper'

class SettingsControllerTest < ActionDispatch::IntegrationTest
    setup do
        @user = create_user
        @sysadmin = create_user(options = { is_sysadmin: true })
    end

    test "should not see settings if not sysadmin" do
        @token = login(@user.email, "1234abcd")

        get '/settings/', headers: { "Authentication": @token }, as: :json

        assert_response :unauthorized
    end

    test "should not update settings if not sysadmin" do
        @token = login(@user.email, "1234abcd")

        patch '/settings/', headers: { "Authentication": @token }, as: :json

        assert_response :unauthorized
    end

    test "should see settings if sysadmin" do
        @token = login(@sysadmin.email, "1234abcd")

        get '/settings/', headers: { "Authentication": @token }, as: :json

        assert_response :success
    end

    test "should update settings if sysadmin" do
        @token = login(@sysadmin.email, "1234abcd")

        patch '/settings/', params: {
            "time_to_change_password": 7,
            "maximum_login_tries": 9,
            "time_to_invalidate_token": 15
        },
        headers: {
            "Authentication": @token
        }, as: :json

        assert_response :success
    end
end
