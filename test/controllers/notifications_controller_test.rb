require 'test_helper'

class NotificationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = create_user
  end

  test "should get index" do
    token = login @user.email, "1234abcd"

    get "/users/#{@user.id}/notifications", headers: { "Authentication": token }, as: :json

    assert_response :success
  end

end
