Rails.application.routes.draw do
  root to: 'application#welcome'

  resources :hospitals
  resources :substances, except: [:create]
  resources :medicines

  resources :users, only: [:create, :index, :show, :update] do
    resources :tasks, only: [:index, :update]
    resource :licenses, only: [:show, :create]
    resources :notifications, only: [:index]
    resources :interactions, except: [:show]
    resources :logs, only: [:index]
  end

  get '/users/:id/medicines/:medicine_id', to: 'users#check_medicine'

  post '/sessions', to: 'application#login'

  get '/settings/', to: 'settings#show'
  patch '/settings/', to: 'settings#update'
end
