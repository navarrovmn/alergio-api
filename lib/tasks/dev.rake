namespace :dev do

    desc "Create some objects to help developing"
    task setup: :environment do

        # Create users
        10.times do |t|
            sysadmin = false

            if t == 9
                sysadmin = true
            end

            User.create!(
                first_name: Faker::Name.first_name,
                email: Faker::Internet.email,
                cpf: CPF.generate,
                birth: Faker::Date.between(35.years.ago, 18.years.ago),
                is_sysadmin: sysadmin,
                password: "1234abcd",
                password_confirmation: "1234abcd",
                last_changed_password_at: Time.now
            )
        end

        # Create medicines and substances
        5.times do |t|
            med = Medicine.create!(
                    name: Faker::Science.element,
                    register: rand.to_s[2..10]
                  )

            med.substances.create(name: Faker::Science.element_symbol)
        end

        # Create license

        license = License.create(
                        license_type: 1,
                        number: "12345678",
                        state: "Distrito Federal",
                        user_id: User.last.id
                    )

        Task.create(
          user_id: 10,
          license_id: license.id
        )

    end

end