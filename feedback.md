## Sugestões, _feedback_ e reclamações

Estamos muito felizes em receber qualquer tipo de _feedback_, sugestões ou alguma reclamação em relação ao funcionamento do sistema. Para isso, mande um e-mail contendo suas ideias e perspectivas para:

* victor.matias.navarro@gmail.com
